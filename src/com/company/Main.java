
class Point {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static double distance(Point from, Point to) {
        double xDistance = to.x - from.x;
        double yDistance = to.y - from.y;
        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }

    public static double direction(Point from, Point to) {
        double xDistance = to.x - from.x;
        double yDistance = to.y - from.y;
        return Math.atan2(yDistance, xDistance);
    }
}


public class Main {

    public static void main(String[] args) {

        Point from = new Point(1.3, 5.4);
        Point to = new Point(2.3, 6.7);

        System.out.println(Point.distance(from, to));
        System.out.println(Point.direction(from, to));
    }
}
